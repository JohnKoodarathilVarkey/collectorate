import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Login from './../pages/LoginPage/login';
import Dashboard from './../pages/Dashboard/dashboard';
import HTable from './../pages/HTable/BillingScreen';
import Nav from './../components/Navbar';
const routing=()=>{
  return(
  <Router>
    <Nav/>
      <Switch>
        <Route exact path="/" component={HTable} />
        
        <Route exact path="/dashboard" component={Dashboard} />

        
      </Switch> 
  
  </Router>
  );
};
export default routing;