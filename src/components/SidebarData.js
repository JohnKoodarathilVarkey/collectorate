import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarData = [
  {
    title: 'Home',
    path: '/',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },
  {
    title: 'Approval List',
    path: '/reports',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Ambulance',
    path: '/products',
    icon: <FaIcons.FaAmbulance />,
    cName: 'nav-text'
  },
  {
    title: 'Transport',
    path: '/team',
    icon: <FaIcons.FaExchangeAlt />,
    cName: 'nav-text'
  },
  {
    title: 'Current Patient',
    path: '/messages',
    icon: <FaIcons.FaBed />,
    cName: 'nav-text'
  },
  
];
