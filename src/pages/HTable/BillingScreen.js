import React from 'react';
import FormComponent from  './formHospital';
import './Billing.css';


export default class BillingScreen extends React.Component{
  constructor(props){
    super(props);
    this.state= {
      Data: [],
      isHelperOpen: false
    }
  }

  styles = {
    Logo:{
      height: 70,
      margin: "5px 10px",
      paddingLeft: "70px",
     
    },
    Form:{
    paddingLeft: "22%",
     margin:'5%',
     width:'40%',
     height:'42px'
     
    },
    navbars:{
      backgroundColor:"#4b4779",
      color: "white",
      display: "flex",
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingTop: "15px",
      width:'100%',
      height :'auto'
    },
    UserAvatarDiv:{
      paddingRight: "70px",
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    Title: {
      display: 'block',
      margin: "10px",
      fontSize: "5vmax",
      fontFamily: 'Roboto',
      textTransform: 'capitalize',
      fontWeight: 'bold',
    },
    AvatarDiv:{
      paddingLeft: "5px",
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'

    },
    Avatar:{
      verticalAlign: 'middle',
      width: "75px",
      height: "75px",
      borderRadius: "50%",
      paddingLeft: "10px",
      position: 'absolute',
    },
    UserName: {
      fontSize: "15px",
      fontFamily: 'Montserrat',
      textTransform: 'capitalize',
    },
   

    
  }

//   update(e) {
//     console.log(e.target.value);
//     e.preventDefault();
//     const Datas = {
//         name:e.target.value
//     }
//     console.log();
//     const row  = this.state.ClientDocument;
//     var id=row.id;
//     axios.patch('http://dcc-pr.herokuapp.com/hospital/'+id+'/?format=json', Datas)
//     .then(res => console.log("res.data",res.data));
//     this.getData();
// }

  render(){    
 
    return(
    <div id="container" style={{height: "170vh", width: "100%",  backgroundColor: 'rgba(133,166,204,0.29)'}}>
   
      <div 
        style={this.styles.navbars}
      >
          <div 
            id="Title-Div"
            style={{ paddingLeft: "80px" }}
          >
              <span 
                className="Title"
                style={this.styles.Title}
              > 
                Hospital Name
              </span>
          </div>
          <div 
            style={this.styles.UserAvatarDiv}
          >
              <span 
                className="UserName"
                style={this.styles.UserName}
              >
                UserName
              </span>
              <div 
                className="Avatar-Div"
                style={this.styles.AvatarDiv}
              >
              
                  <div 
                    style = {{
                      display: this.state.isHelperOpen? 'block' : "none",
                      backgroundColor: "white",
                      position:'relative',
                      width: "80px"
                    }}
                  >
                    <ul
                      style ={{
                        color: "black",
                        listStyleType: 'none',
                        margin: 'auto',
                        marginBlockStart: "0px",
                        paddingInlineStart: "0px"
                      }}
                    >
                      <li>
                        Logout
                      </li>
                    </ul>
                  </div>  
              </div>
          </div>
      </div>
      <div style={this.styles.Form}>
        <FormComponent />
      </div>
    </div>
    
    );
  }
}

