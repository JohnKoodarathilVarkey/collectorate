import React, { Component } from "react";
import Axios from 'axios'
import "./formhsp.css";
import { Row } from "react-bootstrap";


const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};

export default class FormComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        deactive: false,
        id: 0,
        lat: 0,
        long: 0,
        name: "",
        type: "",
        vacancy_general_female: 0,
        vacancy_general_male: 0,
        vacancy_icu: 0,
        vacancy_isolation_female: 0,
        vacancy_isolation_male: 0,
        vacancy_ventilator: 0
      },
      formErrors: {
        Name: "",
        type: "",
        lat: 0,
        long: 0,
        gmale: 0,
        gfemale: 0,
        imale: 0,
        ifemale: 0,
        icu: 0,
        ventilator: 0,
      }
    };
  }

  preEditValues= {
        Name: "",
        type: "",
        lat: 0,
        long: 0,
        gmale: 0,
        gfemale: 0,
        imale: 0,
        ifemale: 0,
        icu: 0,
        ventilator: 0,
        deactive: false
  }

  savePreEditValues(){
    this.preEditValues={...this.state.data};
  }
  revertPreEditValues = event =>{
    this.setState({data:this.preEditValues});
    console.log(this.state.data);
    event.preventDefault();
  }

  handleSubmit = async e => {
    e.preventDefault();
    let body = this.state.data;
    let data = await Axios.patch("http://dcc-pr.herokuapp.com/hospital/1/",body,{
      headers:{
        'Content-Type':"application/json"
      }
    })
    .then(response=>{
      return response.data
    })
    .catch(err=>{
      console.log("err on submit:",err)
    })
    console.log(data);
  };


  getData = async () =>{
    const data = await Axios.get('http://dcc-pr.herokuapp.com/hospital/1') //
    .then(response=>{
      let respdata = response.data
      return respdata;
    })
    .then(data=>{
      return data;
    })
    .catch(err=>{
      console.log(err)
      return {};
    })
    this.setState({data})
    console.log("data",this.state.data);
  }

  async componentDidMount(){
    console.clear();
    await this.getData();
    this.savePreEditValues();
  }

  render() {
    const { formErrors, data } = this.state;

    const checkNumb = (num)=> {
      if(num==NaN || num < 0 )
        return 0
      return num;
    }


    const handleChange = e => {
      e.preventDefault();
      let { name,value }  = e.target;
      let formErrors = { ...this.state.formErrors };
      console.log(name, value, typeof value, (value == 'true') );
      if(name === "name" || name === "type" )
        value = value;
      else if(name === "lat" || name === "long")
        value = checkNumb(parseFloat(value));
      else if( name === "deactive")
        value = (value == 'true');
      else
        value = parseInt(value);
    
      data[name]=value;
      this.setState({ formErrors,data });
      console.log(this.state.data);
    };

    return (      
      <div className="wrFormer">
        <div className="form-wrFormer">
        &nbsp;
          <form noValidate>
            <div className="Name">
              <label htmlFor="Name">Name</label>
              <input
                className={data.name.length <= 0 ? "error" : null}
                placeholder=" Name"
                type="text"
                name="name"
                value={data.name}
                noValidate
                onChange={handleChange}
              />
              {formErrors.Name.length > 0 && (
                <span className="errorMessage">{formErrors.Name}</span>
              )}
            </div>
            <div className="type">
              <label htmlFor="type">Type</label>
              <select name="type" value={data.type} onChange={handleChange}>
                <option value="COVID HOSPITAL">COVID HOSPITAL</option>
                <option value="DCC">DCC</option>
                <option value="CFLTC">CFLTC</option>
                <option value="CSLTC">CSLTC</option>
              </select>
            
            </div>
            
            <div className="number">
              <label htmlFor="number">Lat</label>
              <input
                placeholder="Lat"
                type="number"
                name="lat"
                value={data.lat}
                noValidate
                onChange={handleChange}
              />
             
            </div>
            <div className="number">
              <label htmlFor="number">Long</label>
              <input
             
                placeholder="Long"
                type="number"
                name="long"
                value={data.long}
                noValidate
                onChange={handleChange}
              />
              
            </div>
            <div className="number">
              <label htmlFor="number">Vacancy general male</label>
              <input
                placeholder="General male"
                type="number"
                name="vacancy_general_male"
                value={data.vacancy_general_male}
                noValidate
                onChange={handleChange}
              />
             
            </div>
            <div className="number">
              <label htmlFor="number">Vacancy general female</label>
              <input
                placeholder="General female"
                type="number"
                name="vacancy_general_female"
                value={data.vacancy_general_female}
                noValidate
                onChange={handleChange}
              />
             
            </div>
            <div className="number">
              <label htmlFor="number">Vacancy isolation male</label>
              <input            
                placeholder="Isolation male"
                type="number"
                name="vacancy_isolation_male"
                value={data.vacancy_isolation_male}
                noValidate
                onChange={handleChange}
              />
             
            </div>
            <div className="number">
              <label htmlFor="number">Vacancy isolation female</label>
              <input
             
                placeholder="Isolation female"
                type="number"
                name="vacancy_isolation_female"
                value={data.vacancy_isolation_female}
                noValidate
                onChange={handleChange}
              />
             
            </div>

            
            <div className="number">
              <label htmlFor="number">Vacancy icu</label>
              <input
                placeholder="Vacancy icu"
                type="number"
                name="vacancy_icu"
                value={data.vacancy_icu}
                noValidate
                onChange={handleChange}
              />
             
            </div>
            <div className="number">
              <label htmlFor="number">Vacancy ventilator</label>
              <input
             
                placeholder="Vacancy ventilator"
                type="number"
                name="vacancy_ventilator"
                value={data.vacancy_ventilator}
                noValidate
                onChange={handleChange}
              />
             
            </div>
            <div className="checkbox">
              <label htmlFor="checkbox">Deactive</label>
              <input
                type="checkbox"
                name="deactive"
                value={!data.deactive||false}
                noValidate
                onChange={handleChange}
              />
             
            </div>
            
            
            <div className="upload">
              <button 
                type="submit"
                onClick={this.handleSubmit}
              >
                  Upload
              </button>
              <button
                onClick={this.revertPreEditValues}
              >
                Cancel Changes
              </button>
             
            </div>
          </form>
        </div>
      </div>
    );
  }
}
